create table Project (
                         projectId uuid not null,
                         createdBy uuid,
                         creationTime timestamp,
                         projectCode varchar(255),
                         projectDescription varchar(255),
                         projectName varchar(255),
                         updateTime timestamp,
                         updatedBy uuid,
                         primary key (projectId)
)


