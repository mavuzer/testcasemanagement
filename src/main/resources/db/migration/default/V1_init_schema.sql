create table master_tenants (
                                master_id uuid not null,
                                tenant_id uuid not null
);

create table MasterTenant (
                              master_id uuid not null,
                              name varchar(255),
                              password varchar(255),
                              primary key (master_id)
);

create table role_user (
                           role_id int8 not null,
                           user_id uuid not null
);

create table security_role (
                               id  long not null,
                               description varchar(255),
                               role_name varchar(255),
                               primary key (id)
);

create table tenants (
                         id uuid not null,
                         tenantName varchar(255) not null,
                         primary key (id)
);

create table user_role (
                           user_id uuid not null,
                           role_id int8 not null
);

create table USERS (
                       id uuid not null,
                       password varchar(255),
                       userName varchar(255),
                       tenant_id uuid,
                       primary key (id)
);