package com.netas.visium.testcasemanagement.auth;


import com.netas.visium.testcasemanagement.auth.jwt.JwtTokenProvider;
import com.netas.visium.testcasemanagement.entity.User;
import com.netas.visium.testcasemanagement.service.tenant.TenantService;
import com.netas.visium.testcasemanagement.service.user.UserService;
import com.netas.visium.testcasemanagement.util.TenantContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static org.springframework.security.core.userdetails.User.withUsername;

@Component
public class SecurityUserDetailsService implements UserDetailsService {

    private UserService userService;
    private JwtTokenProvider jwtTokenProvider;

    public SecurityUserDetailsService(UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.findByUsername(s).orElseThrow(() ->
                new UsernameNotFoundException(String.format("User with name %s does not exist", s)));


        return withUsername(user.getUserName())
                .password(user.getPassword())
                .authorities(user.getRoles())
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }


    public Optional<UserDetails> loadUserByJwtToken(String jwtToken) {
        if (jwtTokenProvider.validateToken(jwtToken)) {
            TenantContext.setCurrentTenant(jwtTokenProvider.getTenantName(jwtToken));
            return Optional.of(
                    withUsername(jwtTokenProvider.getUsername(jwtToken))
                            .authorities(jwtTokenProvider.getRoles(jwtToken))
                            .password("") //token does not have password but field may not be empty
                            .accountExpired(false)
                            .accountLocked(false)
                            .credentialsExpired(false)
                            .disabled(false)
                            .build());
        }
        return Optional.empty();
    }

    public Optional<UserDetails> loadUserByJwtTokenAndDatabase(String jwtToken) {
        if (jwtTokenProvider.validateToken(jwtToken)) {
            return Optional.of(loadUserByUsername(jwtTokenProvider.getUsername(jwtToken)));
        } else {
            return Optional.empty();
        }
    }
}
