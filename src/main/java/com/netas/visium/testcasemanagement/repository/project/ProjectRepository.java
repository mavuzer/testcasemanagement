package com.netas.visium.testcasemanagement.repository.project;

import com.netas.visium.testcasemanagement.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
}

