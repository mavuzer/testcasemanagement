package com.netas.visium.testcasemanagement.repository.tenant;

import com.netas.visium.testcasemanagement.entity.tenant.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface TenantRepository extends JpaRepository<Tenant, UUID> {

    Optional<Tenant> findTenantByTenantName(String name);

}
