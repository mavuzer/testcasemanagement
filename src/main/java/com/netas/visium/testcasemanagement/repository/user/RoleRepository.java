package com.netas.visium.testcasemanagement.repository.user;

import com.netas.visium.testcasemanagement.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findRoleByRoleName(String name);

}
