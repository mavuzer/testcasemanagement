package com.netas.visium.testcasemanagement.repository.tenant.master;

import com.netas.visium.testcasemanagement.entity.tenant.master.MasterTenant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MasterTenantRepository extends JpaRepository<MasterTenant, UUID> {
}
