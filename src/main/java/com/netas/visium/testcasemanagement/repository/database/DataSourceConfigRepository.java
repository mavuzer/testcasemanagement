package com.netas.visium.testcasemanagement.repository.database;

import com.netas.visium.testcasemanagement.entity.datasource.DataSourceConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface DataSourceConfigRepository extends JpaRepository<DataSourceConfig, UUID> {

    Optional<DataSourceConfig> findDataSourceConfigByTenantId(UUID tenantId);
}
