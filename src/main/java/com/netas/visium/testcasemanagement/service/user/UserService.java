package com.netas.visium.testcasemanagement.service.user;

import com.netas.visium.testcasemanagement.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    User saveUser(User user);

    Optional<User> findByUsername(String username);

    List<User> findAllUsers();

    Optional<User> findById(UUID userId);
}
