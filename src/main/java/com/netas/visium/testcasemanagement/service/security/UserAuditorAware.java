package com.netas.visium.testcasemanagement.service.security;

import com.netas.visium.testcasemanagement.service.user.UserService;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserAuditorAware implements AuditorAware<UUID> {

    private UserService userService;

    public UserAuditorAware(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<UUID> getCurrentAuditor() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        UUID id = userService.findByUsername(((User) authentication.getPrincipal()).getUsername()).get().getId();

        return Optional.of(id);
    }
}
