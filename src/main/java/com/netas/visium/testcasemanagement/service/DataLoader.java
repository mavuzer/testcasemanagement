package com.netas.visium.testcasemanagement.service;

import com.netas.visium.testcasemanagement.entity.Role;
import com.netas.visium.testcasemanagement.entity.User;
import com.netas.visium.testcasemanagement.entity.tenant.Tenant;
import com.netas.visium.testcasemanagement.entity.tenant.master.MasterTenant;
import com.netas.visium.testcasemanagement.service.tenant.master.MasterTenantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;

    private PasswordEncoder passwordEncoder;
    private MasterTenantService masterTenantService;
    private DataSource dataSource;

    public DataLoader(PasswordEncoder passwordEncoder, MasterTenantService masterTenantService, DataSource dataSource) {
        this.passwordEncoder = passwordEncoder;
        this.masterTenantService = masterTenantService;
        this.dataSource = dataSource;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup)
            return;

        initializeMaster();

    }

    private void initializeMaster() {

        Role tenant_role = Role.builder().roleName("ROLE_ADMIN").description("Role ADMIN could create new users and projects").build();
        List<Role> roles = new ArrayList<>();
        roles.add(tenant_role);
        User user = User.builder().userName("tenant_master").password(passwordEncoder.encode("netas")).roles(roles).build();
        List<User> users = new ArrayList<>();
        users.add(user);
        Tenant tenant = Tenant.builder().tenantName("Tenant1").users(users).build();
        List<Tenant> tenantList = new ArrayList<>();
        tenantList.add(tenant);

        try {
            dataSource.getConnection().createStatement().executeQuery("CREATE SCHEMA IF NOT EXISTS " + tenant.getTenantName());
        } catch (SQLException e) {
            e.printStackTrace();
        }


        MasterTenant masterTenant = MasterTenant.builder().name("Master").password(passwordEncoder.encode("netas")).tenantList(tenantList).build();
        masterTenantService.save(masterTenant);
    }
}
