package com.netas.visium.testcasemanagement.service.tenant;

import com.netas.visium.testcasemanagement.entity.tenant.Tenant;
import com.netas.visium.testcasemanagement.repository.tenant.TenantRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class TenantServiceImpl implements TenantService {

    private TenantRepository tenantRepository;

    public TenantServiceImpl(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Override
    public Tenant save(Tenant tenant) {
        return tenantRepository.save(tenant);
    }

    @Override
    public Optional<Tenant> findById(UUID tenantId) {
        return tenantRepository.findById(tenantId);
    }
}
