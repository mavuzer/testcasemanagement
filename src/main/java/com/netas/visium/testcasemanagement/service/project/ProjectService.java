package com.netas.visium.testcasemanagement.service.project;

import com.netas.visium.testcasemanagement.entity.Project;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectService {

    List<Project> getAllProjects();

    Optional<Project> getProject(UUID id);

    boolean updateProject(UUID id, Project updatedProject);

    boolean deleteProject(UUID id);

    Project addProject(Project project);

}
