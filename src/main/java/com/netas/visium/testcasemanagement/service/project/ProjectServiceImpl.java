package com.netas.visium.testcasemanagement.service.project;

import com.netas.visium.testcasemanagement.entity.Project;
import com.netas.visium.testcasemanagement.repository.project.ProjectRepository;
import com.netas.visium.testcasemanagement.service.security.UserAuditorAware;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;
    private UserAuditorAware userAuditorAware;

    public ProjectServiceImpl(ProjectRepository projectRepository,UserAuditorAware userAuditorAware) {
        this.projectRepository = projectRepository;
        this.userAuditorAware = userAuditorAware;
    }

    @Override
    public List<Project> getAllProjects() {

        return projectRepository.findAll();
    }

    @Override
    public Optional<Project> getProject(UUID id) {
        return projectRepository.findById(id);
    }

    @Override
    public boolean updateProject(UUID id, Project updatedProject) {
        Optional<Project> project = projectRepository.findById(id);

        if (project.isPresent()) {
            project.get().setProjectName(updatedProject.getProjectName());
            project.get().setProjectDescription(updatedProject.getProjectDescription());
            project.get().setUpdatedBy(userAuditorAware.getCurrentAuditor().orElse(null));
            project.get().setUpdateTime(LocalDateTime.now());
            projectRepository.save(project.get());
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteProject(UUID id) {

        Optional<Project> project = projectRepository.findById(id);

        if (project.isPresent()) {
            projectRepository.delete(project.get());
            return true;
        } else {
            return false;
        }

    }

    @Override
    public Project addProject(Project project) {

        return projectRepository.save(Project
                .builder()
                .projectCode("PRJ_" + (getAllProjects().size() + 1))
                .projectName(project.getProjectName())
                .projectDescription(project.getProjectDescription())
                .createdBy(userAuditorAware.getCurrentAuditor().orElse(null))
                .updatedBy(userAuditorAware.getCurrentAuditor().orElse(null))
                .creationTime(LocalDateTime.now())
                .updateTime(LocalDateTime.now())
                .build());
    }
}
