package com.netas.visium.testcasemanagement.service.tenant.master;

import com.netas.visium.testcasemanagement.entity.tenant.master.MasterTenant;
import com.netas.visium.testcasemanagement.repository.tenant.master.MasterTenantRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MasterTenantImpl implements MasterTenantService{

    private MasterTenantRepository masterTenantRepository;
    private PasswordEncoder passwordEncoder;

    public MasterTenantImpl(MasterTenantRepository masterTenantRepository,PasswordEncoder passwordEncoder) {
        this.masterTenantRepository = masterTenantRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public MasterTenant save(MasterTenant masterTenant) {
        masterTenant.setPassword(passwordEncoder.encode(masterTenant.getPassword()));
        return masterTenantRepository.save(masterTenant);
    }
}
