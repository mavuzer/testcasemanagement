package com.netas.visium.testcasemanagement.service.user.role;

import com.netas.visium.testcasemanagement.entity.Role;

import java.util.List;
import java.util.Optional;

public interface RoleService {

    Role saveRole(Role role);

    Optional<Role> findRoleByName(String roleName);

    List<Role> findAllRoles();

}
