package com.netas.visium.testcasemanagement.service.datasource;

import com.netas.visium.testcasemanagement.entity.datasource.DataSourceConfig;

import java.util.Optional;
import java.util.UUID;

public interface DataSourceService {

    DataSourceConfig saveDataSource(DataSourceConfig dataSourceConfig);

    Optional<DataSourceConfig> findByTenantId(UUID tenantId);

}
