package com.netas.visium.testcasemanagement.service.multitenant;

import com.netas.visium.testcasemanagement.entity.tenant.MultiTenantConstants;
import com.netas.visium.testcasemanagement.util.TenantContext;
import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import javax.sql.DataSource;
import java.sql.SQLException;

@Component("multiTenantProvider")
public class SchemaMultiTenantConnectionProvider implements MultiTenantConnectionProvider {

    private DataSource dataSource;

    public SchemaMultiTenantConnectionProvider(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Connection getAnyConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public void releaseAnyConnection(Connection connection) throws SQLException {
        connection.close();
    }

    @Override
    public Connection getConnection(String tenantIdentifie) throws SQLException {
        String tenantIdentifier = TenantContext.getCurrentTenant();

        final Connection connection = getAnyConnection();
        try {
            if (tenantIdentifier !=null){
                connection.setSchema(tenantIdentifier);
            }else {
                connection.setSchema(MultiTenantConstants.DEFAULT_TENANT_ID);
                
            }
        }catch (SQLException e){
            throw new HibernateException("Problem setting schema to " + tenantIdentifier, e);
        }finally {
            connection.createStatement().close();
        }

        return connection;
    }

    @Override
    public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
        try {
            connection.setSchema(MultiTenantConstants.DEFAULT_TENANT_ID);
        }
        catch ( SQLException e ) {
            throw new HibernateException(
                    "Problem setting schema to " + tenantIdentifier,
                    e
            );
        }
        connection.close();
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return true;
    }

    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> unwrapType) {
        return null;
    }
}
