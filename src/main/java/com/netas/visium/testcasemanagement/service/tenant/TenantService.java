package com.netas.visium.testcasemanagement.service.tenant;

import com.netas.visium.testcasemanagement.entity.tenant.Tenant;

import java.util.Optional;
import java.util.UUID;

public interface TenantService {

    Tenant save(Tenant tenant);

    Optional<Tenant> findById(UUID tenantId);
}
