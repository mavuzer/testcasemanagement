package com.netas.visium.testcasemanagement.service.datasource;

import com.netas.visium.testcasemanagement.util.TenantContext;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

@Component("tenantIdentifier")
public class CurrentTenantIdentifierResolverImpl implements CurrentTenantIdentifierResolver {

    @Override
    public String resolveCurrentTenantIdentifier() {
        if (TenantContext.getCurrentTenant() == null) {
            return "visium";
        } else return TenantContext.getCurrentTenant();
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return false;
    }
}
