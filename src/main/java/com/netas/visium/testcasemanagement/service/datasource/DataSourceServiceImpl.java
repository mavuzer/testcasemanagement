package com.netas.visium.testcasemanagement.service.datasource;

import com.netas.visium.testcasemanagement.entity.datasource.DataSourceConfig;
import com.netas.visium.testcasemanagement.repository.database.DataSourceConfigRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class DataSourceServiceImpl implements DataSourceService {

    private DataSourceConfigRepository dataSourceConfigRepository;

    public DataSourceServiceImpl(DataSourceConfigRepository dataSourceConfigRepository) {
        this.dataSourceConfigRepository = dataSourceConfigRepository;
    }

    @Override
    public DataSourceConfig saveDataSource(DataSourceConfig dataSourceConfig) {
        return dataSourceConfigRepository.save(dataSourceConfig);
    }

    @Override
    public Optional<DataSourceConfig> findByTenantId(UUID tenantId) {
        return dataSourceConfigRepository.findDataSourceConfigByTenantId(tenantId);
    }
}
