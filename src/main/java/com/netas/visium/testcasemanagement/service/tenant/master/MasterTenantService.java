package com.netas.visium.testcasemanagement.service.tenant.master;

import com.netas.visium.testcasemanagement.entity.tenant.master.MasterTenant;

public interface MasterTenantService {

    MasterTenant save(MasterTenant masterTenant);
}
