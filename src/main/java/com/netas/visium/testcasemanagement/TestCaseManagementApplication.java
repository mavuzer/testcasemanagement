package com.netas.visium.testcasemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.netas.visium.testcasemanagement.repository")
public class TestCaseManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCaseManagementApplication.class, args);
    }

}
