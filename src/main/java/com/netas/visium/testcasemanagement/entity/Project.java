package com.netas.visium.testcasemanagement.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Slf4j
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "projectId", nullable = false)
    private UUID projectId;

    @Column
    private String projectCode;

    @Column
    private String projectName;

    @Column
    private String projectDescription;

    @Column
    @CreationTimestamp
    private LocalDateTime creationTime;

    @Column
    @UpdateTimestamp
    private LocalDateTime updateTime;

    @Column
    @CreatedBy
    private UUID createdBy;

    @Column
    @LastModifiedBy
    private UUID updatedBy;


}
