package com.netas.visium.testcasemanagement.entity.tenant;

public interface MultiTenantConstants {
    String DEFAULT_TENANT_ID = "visium";
}
