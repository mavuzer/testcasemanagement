package com.netas.visium.testcasemanagement.entity.tenant;

import com.netas.visium.testcasemanagement.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Entity(name = "Tenants")
@Table(name = "tenants")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tenant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    private String tenantName;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "tenant_id")
    private List<User> users;

}
