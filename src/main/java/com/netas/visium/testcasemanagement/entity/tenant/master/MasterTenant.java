package com.netas.visium.testcasemanagement.entity.tenant.master;

import com.netas.visium.testcasemanagement.entity.tenant.Tenant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MasterTenant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID master_id;

    private String name;

    private String password;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "master_tenants", joinColumns = @JoinColumn(name = "master_id")
            , inverseJoinColumns = @JoinColumn(name = "tenant_id"))
    private List<Tenant> tenantList;

}
