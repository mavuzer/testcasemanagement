package com.netas.visium.testcasemanagement.controller;

import com.netas.visium.testcasemanagement.entity.Project;
import com.netas.visium.testcasemanagement.service.project.ProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("/api/projects")
public class ProjectController {

    private ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(value = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USER')")
    @ResponseBody
    public List<Project> getAllProjects() {
        return projectService.getAllProjects();
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    @ResponseBody
    public ResponseEntity getProject(@PathVariable(name = "id") UUID id) {

        Optional<Project> project = projectService.getProject(id);

        if (project.isPresent()) {
            return ResponseEntity.ok(project.get());
        } else {
            return new ResponseEntity<>("Project could not found!", HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping(value = "/update-project")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity updateProject(@RequestBody Project project) {

        if (projectService.updateProject(project.getProjectId(), project)) {
            return ResponseEntity.ok("Project successfully updated!");
        } else {
            return new ResponseEntity<>("Project could not updated!", HttpStatus.BAD_REQUEST);
        }

    }

    @DeleteMapping(value = "/delete-project")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteProject(@RequestParam("id") String id) {

        if (projectService.deleteProject(UUID.fromString(id))) {
            return ResponseEntity.ok("Project successfully deleted!");
        } else {
            return new ResponseEntity<>("Project could not deleted!", HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping(value = "/create-project",produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
    public Project createProject(@RequestBody Project project) {

        return projectService.addProject(project);

    }

}
