package com.netas.visium.testcasemanagement.controller;

import com.netas.visium.testcasemanagement.auth.jwt.JwtTokenProvider;
import com.netas.visium.testcasemanagement.entity.AuthenticationRequest;
import com.netas.visium.testcasemanagement.entity.User;
import com.netas.visium.testcasemanagement.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Slf4j
public class AuthenticationController {

    private AuthenticationManager authenticationManager;
    private JwtTokenProvider jwtTokenProvider;
    private UserService userService;

    public AuthenticationController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
    }

    @PostMapping("/auth/signin")
    public ResponseEntity signin(@RequestBody AuthenticationRequest data) {

        Optional<String> token;
        Optional<User> user = userService.findByUsername(data.getUsername());
        if (user.isPresent()) {
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));
                token = Optional.of(jwtTokenProvider.createToken(data.getUsername(), user.get().getRoles(), user.get().getTenant().getTenantName()));
                Map<Object, Object> model = new HashMap<>();
                model.put("username", data.getUsername());
                model.put("token", token);
                return ResponseEntity.ok(model);
            } catch (AuthenticationException e) {
                log.info("Log in failed for user {}", data.getUsername());
            }
        }
        return new ResponseEntity<>("Invalid user name or password!", HttpStatus.BAD_REQUEST);
    }
}
