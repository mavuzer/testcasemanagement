package com.netas.visium.testcasemanagement.controller;

import com.netas.visium.testcasemanagement.entity.User;
import com.netas.visium.testcasemanagement.service.security.UserAuditorAware;
import com.netas.visium.testcasemanagement.service.user.UserService;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;
    private UserAuditorAware userAuditorAware;

    public UserController(UserService userService,UserAuditorAware userAuditorAware) {
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/all",produces = MediaType.APPLICATION_JSON_VALUE)
    private List<User> getAllUsers(){
        return userService.findAllUsers();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/create-user",produces = MediaType.APPLICATION_JSON_VALUE)
    private User createUser(@RequestBody User user){

        Optional<User> currentUser = null;
        if (userAuditorAware.getCurrentAuditor().isPresent()){
            currentUser = userService.findById(userAuditorAware.getCurrentAuditor().get());
        }

        User usr = User.builder().userName(user.getUserName())
                .password(user.getPassword())
                .roles(user.getRoles())
                .tenant(currentUser.get().getTenant()).build();


        return userService.saveUser(usr);
    }



}
