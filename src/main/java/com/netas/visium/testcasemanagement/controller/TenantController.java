package com.netas.visium.testcasemanagement.controller;

import com.netas.visium.testcasemanagement.service.tenant.TenantService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
public class TenantController {

    private TenantService tenantService;

    public TenantController(TenantService tenantService) {
        this.tenantService = tenantService;
    }

//    @PreAuthorize("hasRole('Super_Admin')")
//    @PostMapping(value = "/create-tenant",produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity createTenant(@RequestParam String tenantName){
//        if (tenantService.createTenant(tenantName)!= null){
//            return ResponseEntity.ok("Tenant successfully crated");
//        }else return new ResponseEntity("Tenant could not created", HttpStatus.BAD_REQUEST);
//    }

}
