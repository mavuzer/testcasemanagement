package com.netas.visium.testcasemanagement.exceptions;

public class UserNameNotFoundException extends Exception {

    public UserNameNotFoundException(String e){
        super(e);
    }
}

