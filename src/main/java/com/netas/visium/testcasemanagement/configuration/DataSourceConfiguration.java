package com.netas.visium.testcasemanagement.configuration;

import com.netas.visium.testcasemanagement.TestCaseManagementApplication;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DataSourceConfiguration {

    @Value("${custom.database.class-name}")
    private String className;

    @Value("${custom.database.url}")
    private String url;

    @Value("${custom.database.user-name}")
    private String userName;

    @Value("${custom.database.password}")
    private String password;

    @Bean
    @Primary
    public DataSource getDataSource(){
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(className);
        dataSourceBuilder.url(url);
        dataSourceBuilder.username(userName);
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }

//    @Bean
//    public JpaVendorAdapter jpaVendorAdapter(){ return new HibernateJpaVendorAdapter(); }
//
//    @Bean
//    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource, JpaProperties jpaProperties,
//                                                                           @Qualifier("multiTenantProvider") MultiTenantConnectionProvider multiTenantConnectionProvider,
//                                                                           @Qualifier("tenantIdentifier") CurrentTenantIdentifierResolver tenantIdentifierResolver){
//        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//        em.setDataSource(dataSource);
//        em.setPackagesToScan(TestCaseManagementApplication.class.getPackage().getName());
//        em.setJpaVendorAdapter(jpaVendorAdapter());
//
//        Map<String, Object> jpaPropertiesMap = new HashMap<>(jpaProperties.getProperties());
//        jpaPropertiesMap.put(Environment.MULTI_TENANT, MultiTenancyStrategy.SCHEMA);
//        jpaPropertiesMap.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProvider);
//        jpaPropertiesMap.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, tenantIdentifierResolver);
//        em.setJpaPropertyMap(jpaPropertiesMap);
//
//        return em;
//    }

}
